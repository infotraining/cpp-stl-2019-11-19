#include <algorithm>
#include <bitset>
#include <iostream>
#include <numeric>
#include <queue>
#include <stack>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

TEST_CASE("stack")
{
    cout << "stack:\n";
    stack<string> words;

    words.push("one");
    words.push("two");
    words.push("three");

    while (!words.empty())
    {
        string item = words.top();
        words.pop();

        cout << item << "\n";
    }
}

TEST_CASE("queue")
{
    cout << "\nq:\n";

    queue<string> q;

    q.push("one");
    q.push("two");
    q.push("three");

    while (!q.empty())
    {
        auto item = q.front();
        q.pop();

        cout << item << "\n";
    }
}

TEST_CASE("priority q")
{
    priority_queue<int, vector<int>, greater<>> pq;

    pq.push(1);
    pq.push(9);
    pq.push(7);

    cout << pq.top() << "\n";
    pq.pop();
}

TEST_CASE("bitset")
{
    bitset<16> bs1 {35};
    bitset<16> bs2 {"010101110111"};

    REQUIRE(bs1.test(1));

    bs1.flip(0);
    cout << "bs1: " << bs1 << " - " << bs1.to_ulong() << endl;

    bs1.flip();
    cout << "bs1: " << bs1 << " - " << bs1.to_ulong() << endl;

    cout << "result: " << (~bs1 | bs2) << endl;
}
