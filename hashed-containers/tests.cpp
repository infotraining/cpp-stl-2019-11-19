#include <algorithm>
#include <boost/functional/hash.hpp>
#include <iostream>
#include <numeric>
#include <random>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

TEST_CASE("unordered containers")
{
    unordered_set<int> values = {4, 142, 634, 665, 42, 2453, 55};

    if (auto pos = values.find(42); pos != values.end())
        cout << *pos << " has been found\n";

    auto [pos, was_inserted] = values.insert(67);

    Utils::print(values, "values");
}

template <typename HashedContainer>
void print_unordered(const HashedContainer& container, string_view prefix)
{
    cout << prefix << ": "
         << "size: " << container.size()
         << "; bucket_count: " << container.bucket_count()
         << "; max_load_factor: " << container.max_load_factor()
         << "; load_factor: " << container.load_factor() << "\n";
}

TEST_CASE("optimizing unordered container")
{
    random_device rd;
    mt19937 rnd_gen(rd());
    uniform_int_distribution<> rnd_dstr(0, 100);

    unordered_set<int> uset;

    uset.max_load_factor(7);

    print_unordered(uset, "uset");

    for (int i = 0; i < 64; ++i)
    {
        auto item = rnd_dstr(rnd_gen);
        cout << "item: " << item << (uset.insert(item).second ? " was inserted" : " already in uset") << "\n";
        print_unordered(uset, "uset");
    }

    uset.rehash(199);

    cout << "\nDetailed stat:\n";
    for (size_t i = 0; i < uset.bucket_count(); ++i)
    {
        cout << "Bucket " << i << ": [ ";
        for (auto it = uset.begin(i); it != uset.end(i); ++it)
        {
            cout << *it << " ";
        }
        cout << "]\n";
    }
}

TEST_CASE("hashing")
{
    std::hash<string> string_hasher;

    cout << "hashed(abc): " << string_hasher("abc") << "\n";
}

struct Person
{
    string first_name;
    string last_name;
    int age;

    auto tied() const
    {

        return tie(first_name, last_name, age);
    }

    bool
    operator==(const Person& other) const
    {
        return tied() == other.tied();
    }
};

auto combine_hash = [](const auto&... args) {
    size_t seed {};

    (boost::hash_combine(seed, args), ...);

    return seed;
};

namespace std
{
    template <>
    struct hash<Person>
    {
        size_t operator()(const Person& p) const
        {
            //            size_t seed {};
            //            boost::hash_combine(seed, p.first_name);
            //            boost::hash_combine(seed, p.last_name);
            //            boost::hash_combine(seed, p.age);

            return std::apply(combine_hash, p.tied());
        }
    };
}

TEST_CASE("hashing custom types")
{
    unordered_set<Person> people;

    people.insert(Person {"Jan", "Kowalski", 42});
    people.insert(Person {"Adam", "Nowak", 33});
    people.insert(Person {"Ewa", "Kowalska", 41});

    REQUIRE(people.count(Person {"Jan", "Kowalski", 42}) == 1);
}
