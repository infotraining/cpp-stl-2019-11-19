#include <celero/Celero.h>

#include <algorithm>
#include <deque>
#include <fstream>
#include <iostream>
#include <list>
#include <random>
#include <vector>

#include "test_helpers.hpp"

CELERO_MAIN

using namespace std;

constexpr int no_of_ints = 1'000'000;
constexpr int no_of_objects = 10'000;
constexpr int no_of_samples = 20;
constexpr int no_of_iterations = 10;

using namespace TestsHelpers;

//#define PUSH_BACK_INTS
//#define SORTING_INTS
//#define PUSH_BACKS_OBJECTS
#define SORTING_OBJECTS

//////////////////////////////////////////////////////////////
//
// PUSH_BACKS of ints
//

class IntContainerFixture : public celero::TestFixture
{
protected:
    std::vector<int> data;

    IntContainerFixture() = default;

    std::vector<celero::TestFixture::ExperimentValue> getExperimentValues() const override
    {
        return {{no_of_ints}};
    }

    void setUp(const celero::TestFixture::ExperimentValue& experimentValue) override
    {
        random_device rd;
        mt19937_64 rnd{rd()};

        data.reserve(static_cast<size_t>(experimentValue.Value));

        for (int64_t i = 0; i < experimentValue.Value; ++i)
        {
            data.push_back(rnd_int(rnd, 0, 100'000));
        }
    }

    void tearDown() override
    {
        data.clear();
    }
};

#ifdef PUSH_BACK_INTS

BASELINE_F(PushBackInts, vector_int_pb_rsrv, IntContainerFixture, no_of_samples, no_of_iterations)
{
    vector<int> vec_int;
    vec_int.reserve(data.size());

    for (const auto& item : data)
        vec_int.push_back(item);
}

BENCHMARK_F(PushBackInts, vector_int_pb, IntContainerFixture, no_of_samples, no_of_iterations)
{
    vector<int> vec_int;

    for (const auto& item : data)
        vec_int.push_back(item);
}

BENCHMARK_F(PushBackInts, deque_int_pb, IntContainerFixture, no_of_samples, no_of_iterations)
{
    deque<int> dq_int;

    for (const auto& item : data)
        dq_int.push_back(item);
}

BENCHMARK_F(PushBackInts, list_int_pb, IntContainerFixture, no_of_samples, no_of_iterations)
{
    list<int> lst_int;

    for (const auto& item : data)
        lst_int.push_back(item);
}

#endif

//////////////////////////////////////////////////////////////
//
// SORTING of ints
//

template <template <class, class> typename Container, template <class> typename Allocator = std::allocator>
class SortIntContainerFixture : public IntContainerFixture
{
protected:
    Container<int, Allocator<int>> unsorted_data;

    SortIntContainerFixture() = default;

    void setUp(const celero::TestFixture::ExperimentValue& experimentValue) override
    {
        IntContainerFixture::setUp(experimentValue.Value);

        unsorted_data.assign(data.begin(), data.end());
    }

    void tearDown() override
    {
        unsorted_data.clear();
        IntContainerFixture::tearDown();
    }
};

#ifdef SORTING_INTS

BASELINE_F(SortInts, sort_vec, SortIntContainerFixture<vector>, no_of_samples, no_of_iterations)
{
    sort(unsorted_data.begin(), unsorted_data.end());
}

BENCHMARK_F(SortInts, sort_dq, SortIntContainerFixture<deque>, no_of_samples, no_of_iterations)
{
    sort(unsorted_data.begin(), unsorted_data.end());
}

BENCHMARK_F(SortInts, sort_lst, SortIntContainerFixture<list>, no_of_samples, no_of_iterations)
{
    unsorted_data.sort();
}

#endif

//////////////////////////////////////////////////////////////
//
// PUSH_BACKS of objects
//

//------------------------------------------------------------------------------------------------------------
// Tests for objects



class ObjectsContainerFixture : public celero::TestFixture
{
protected:
    std::vector<Tester> data;

public:
    ObjectsContainerFixture() = default;

    std::vector<celero::TestFixture::ExperimentValue> getExperimentValues() const override
    {
        return {{no_of_objects}};
    }

    void setUp(const celero::TestFixture::ExperimentValue& experimentValue) override
    {
        random_device rd;
        mt19937_64 rnd{rd()};

        for (int64_t i = 0; i < experimentValue.Value; ++i)
        {
            data.push_back(Tester{rnd_string(rnd), rnd_string(rnd), rnd_vector(rnd)});
        }
    }

    void tearDown() override
    {
        data.clear();
    }
};

#ifdef PUSH_BACKS_OBJECTS

BASELINE_F(PushBackObjs, vec_obj_pb_rsrv, ObjectsContainerFixture, no_of_samples, no_of_iterations)
{
    vector<Tester> vec;
    vec.reserve(data.size());

    for (const auto& item : data)
        vec.push_back(item);
}

BENCHMARK_F(PushBackObjs, vec_obj_pb, ObjectsContainerFixture, no_of_samples, no_of_iterations)
{
    vector<Tester> vec;

    for (const auto& item : data)
        vec.push_back(item);
}

BENCHMARK_F(PushBackObjs, dq_opj_pb, ObjectsContainerFixture, no_of_samples, no_of_iterations)
{
    deque<Tester> dq;

    for (const auto& item : data)
        dq.push_back(item);
}

BENCHMARK_F(PushBackObjs, lst_opj_pb, ObjectsContainerFixture, no_of_samples, no_of_iterations)
{
    list<Tester> lst;

    for (const auto& item : data)
        lst.push_back(item);
}

#endif

//////////////////////////////////////////////////////////////
//
// SORTING of objects
//

template <template <class, class> typename Container, template <class> typename Allocator = std::allocator>
class SortObjectsContainerFixture : public ObjectsContainerFixture
{
protected:
    Container<Tester, Allocator<Tester>> unsorted_data;

    SortObjectsContainerFixture() = default;

    void setUp(const celero::TestFixture::ExperimentValue& experimentValue) override
    {
        ObjectsContainerFixture::setUp(experimentValue);

        unsorted_data.assign(data.begin(), data.end());
    }

    void tearDown() override
    {
        unsorted_data.clear();
        ObjectsContainerFixture::tearDown();
    }
};

#ifdef SORTING_OBJECTS

BASELINE_F(SortObjs, sort_vec, SortObjectsContainerFixture<vector>, no_of_samples, no_of_iterations)
{
    sort(unsorted_data.begin(), unsorted_data.end());
}

BENCHMARK_F(SortObjs, sort_dq, SortObjectsContainerFixture<deque>, no_of_samples, no_of_iterations)
{
    sort(unsorted_data.begin(), unsorted_data.end());
}

BENCHMARK_F(SortObjs, sort_lst, SortObjectsContainerFixture<list>, no_of_samples, no_of_iterations)
{
    unsorted_data.sort();
}

#endif
