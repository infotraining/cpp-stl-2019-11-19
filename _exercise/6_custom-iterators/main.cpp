#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;
using namespace Catch::Matchers;

class Range
{
    const int start_;
    const int end_;

public:
    class RangeIterator
    {
        int pos_;

    public:
        explicit RangeIterator(int pos)
            : pos_ {pos}
        {
        }

        const int& operator*() const
        {
            return pos_;
        }

        RangeIterator& operator++() // ++it
        {
            ++pos_;
            return *this;
        }

        const RangeIterator operator++(int) //it++
        {
            RangeIterator tmp {*this};
            ++pos_;
            return tmp;
        }

        bool operator==(const RangeIterator& other) const
        {
            return pos_ == other.pos_;
        }

        bool operator!=(const RangeIterator& other) const
        {
            return !(*this == other);
        }
    };

    using iterator = RangeIterator;
    using const_iterator = RangeIterator;

    Range(int start, int end)
        : start_ {start}
        , end_ {end}
    {
    }

    const_iterator begin() const
    {
        return RangeIterator {start_};
    }

    const_iterator end() const
    {
        return RangeIterator {end_};
    }
};

TEST_CASE("Range")
{
    vector<int> data;

    SECTION("empty range")
    {
        //        for(const auto& item : Range{1, 1})
        //        {
        //            data.push_back(item);
        //        }

        auto&& coll = Range {1, 1};

        auto first = coll.begin();
        auto last = coll.end();
        for (auto it = first; it != last; ++it)
        {
            const auto& item = *it;
            data.push_back(item);
        }

        REQUIRE(data.empty());
    }

    SECTION("many items")
    {
        for(const auto& item : Range{1, 10})
        {
            data.push_back(item);
        }

        REQUIRE_THAT(data, Equals(vector<int>{1, 2, 3, 4, 5, 6, 7, 8, 9})); 
    }
}
