#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <regex>
#include <set>
#include <string>
#include <unordered_set>
#include <vector>

using namespace std;

vector<std::string> regex_split(const std::string& s, std::string regex_str = R"(\s+)")
{
    std::vector<std::string> words;

    std::regex rgx(regex_str);

    std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
    std::sregex_token_iterator end;

    while (iter != end)
    {
        words.push_back(*iter);
        ++iter;
    }

    return words;
}

int main()
{
    // wczytaj zawartość pliku en.dict ("słownik języka angielskiego")    
    // sprawdź poprawość pisowni następującego zdania:    
    string input_text = "this is an exmple of very badd snetence";
    vector<string> words = regex_split(input_text);

    ifstream file_dict("en.dict");

    if (!file_dict)
        exit(1);

    auto start = chrono::high_resolution_clock::now();

    unordered_set<string> dict(istream_iterator<string> {file_dict}, istream_iterator<string> {}, 444487);
    dict.max_load_factor(0.5);

    auto middle = chrono::high_resolution_clock::now();

    vector<string> misspelled;

    auto is_misspelled = [&dict](const auto& word) {
        return !dict.count(word);
    };

    copy_if(begin(words), end(words), back_inserter(misspelled), is_misspelled);

    auto stop = chrono::high_resolution_clock::now();

    cout << "Time (load) ms: " << chrono::duration_cast<chrono::milliseconds>(middle - start).count() << "\n";
    cout << "Time (search) us: " << chrono::duration_cast<chrono::microseconds>(stop - middle).count() << "\n";

    for (const auto& word : misspelled)
        cout << word << " ";
    cout << "\n";
}
