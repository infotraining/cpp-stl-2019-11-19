#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <unordered_map>

using namespace std;

std::optional<std::string> load_file_content(const string& filename)
{
    std::ifstream in(filename);

    if (!in)
        exit(1);

    if (in)
    {
        std::string content;

        for (std::string line; std::getline(in, line);)
            content += line;

        return std::move(content);
    }

    else
        return nullopt;
}

vector<string> split_words(const string& text)
{
    vector<string> words;

    boost::tokenizer<> tokens(text);

    for (const auto& word : tokens)
    {
        words.push_back(boost::to_lower_copy(word));
    }

    return words;
}

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym. Wyswietl 20 najczęściej występujących slow (w kolejności malejącej).
*/

int main()
{
    const string file_name = "proust.txt";

    const auto book_content = load_file_content(file_name).value_or("");

    const vector<string> words = split_words(book_content);

    auto start = chrono::high_resolution_clock().now();

    auto make_concordance = [](const auto& words) {
        unordered_map<string, unsigned int> concordance(53'201);

        for (const auto& word : words)
            concordance[word]++;

        return concordance;
    };

    auto make_rating = [](const auto& concordance) {
        multimap<unsigned int, string, greater<>> rating;

        for (const auto& [word, count] : concordance)
            rating.emplace(count, word);

        return rating;
    };

    auto rating = make_rating(make_concordance(words));

    auto stop = chrono::high_resolution_clock().now();

    cout << "Time: " << chrono::duration_cast<chrono::milliseconds>(stop - start).count() << endl;

    for (auto [it, n] = tuple {rating.begin(), 0}; n < 20; ++it, ++n)
    {
        cout << it->second << " - " << it->first << "\n";
    }
}
