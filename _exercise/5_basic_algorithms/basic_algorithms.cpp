#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <sstream>
#include <vector>

using namespace std;

TEST_CASE("algorithm exercise")
{
    using namespace Catch::Matchers;

    vector<int> data = {1, 6, 3, 5, 8, 9, 13, 12, 10, 45};

    auto is_even = [](int x) { return x % 2 == 0; };

    SECTION("count even numbers")
    {
        auto evens_count = count_if(begin(data), end(data), is_even);

        REQUIRE(evens_count == 4);
    }

    SECTION("copy evens to vector")
    {
        vector<int> evens;

        copy_if(begin(data), end(data), back_inserter(evens), is_even);

        REQUIRE_THAT(evens, Equals(vector<int>{6, 8, 12, 10}));

        SECTION("copy evens to stream")
        {
            stringstream out;

            copy(begin(evens), end(evens), ostream_iterator<int>(out, ";"));

            REQUIRE(out.str() == "6;8;12;10;");
        }
    }

    SECTION("create container with squares")
    {
        vector<int> squares(data.size());

        //transform(begin(data), end(data), begin(squares), [](int x) { return x * x; });
        transform(begin(data), end(data), begin(data), begin(squares), multiplies {});

        REQUIRE_THAT(squares, Equals(vector<int>{1, 36, 9, 25, 64, 81, 169, 144, 100, 2025}));
    }

    SECTION("remove from container items divisible by any number from a given array")
    {
        const array<int, 3> eliminators = {{3, 5, 7}};

        auto new_end = remove_if(begin(data), end(data),
            [&eliminators](int from_data) { return any_of(begin(eliminators), end(eliminators),
                                                [from_data](int from_elim) { return from_data % from_elim == 0; }); });
        data.erase(new_end, end(data));

        REQUIRE_THAT(data, Equals(vector<int>{1, 8, 13}));
    }

    SECTION("calculate average")
    {
        auto sum = 0.0;
        double avg = accumulate(begin(data), end(data), 0.0) / size(data);

        REQUIRE(avg == Approx(11.2));

        SECTION("create two containers - 1st with numbers less or equal to average & 2nd with numbers greater than average")
        {
            vector<int> less_equal_than_avg;
            vector<int> greater_than_avg;

            partition_copy(begin(data), end(data),
                back_inserter(less_equal_than_avg),
                back_inserter(greater_than_avg), [avg](int x) { return x <= avg; });

            REQUIRE_THAT(less_equal_than_avg, Contains(vector<int>{1, 6, 3, 5, 8, 9, 10}));
            REQUIRE_THAT(greater_than_avg, Contains(vector<int>{13, 12, 45}));
        }
    }

    SECTION("sort data in descending order")
    {
        sort(begin(data), end(data), greater {});

        REQUIRE(is_sorted(data.rbegin(), data.rend()));

        SECTION("insert a value preserving order of elements")
        {
            int value = 7;

            auto pos = upper_bound(begin(data), end(data), value, greater {});
            data.insert(pos, value);

            REQUIRE(is_sorted(data.rbegin(), data.rend()));
            REQUIRE(binary_search(data.begin(), data.end(), value, greater<int>{}));
        }
    }
}
