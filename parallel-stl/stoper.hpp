#ifndef STOPER_HPP
#define STOPER_HPP

#include <iostream>
#include <chrono>
#include <string>

namespace utils
{
    template <typename F>
    class Stoper
    {
        std::string description_;
        std::chrono::high_resolution_clock::time_point start_;
        F func_;

    public:
        template <typename Func>
        Stoper(const std::string& description, Func&& func)
            : description_{description}
            , func_{std::forward<Func>(func)}
        {
            start_ = std::chrono::high_resolution_clock::now();
            func_();
        }

        Stoper(const Stoper&) = delete;
        Stoper& operator=(const Stoper&) = delete;

        Stoper(Stoper&&) = default;
        Stoper& operator=(Stoper&&) = default;

        ~Stoper()
        {
            auto end = std::chrono::high_resolution_clock::now();

            auto time_interval = std::chrono::duration_cast<std::chrono::milliseconds>(end - start_);

            std::cout << description_ << " - time: " << time_interval.count() << "ms" << std::endl;
        }
    };

    template <typename Func>
    Stoper(const std::string& description, Func&& func)->Stoper<std::decay_t<Func>>;

    template <typename Func>
    auto BENCHMARK(const std::string& description, Func&& func)
    {
        return Stoper(description, std::forward<Func>(func));
    }
}

#endif