#include "stoper.hpp"
#include <algorithm>
#include <cassert>
#include <cctype>
#include <chrono>
#include <execution>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <numeric>
#include <optional>
#include <random>
#include <string>

using namespace std;
using namespace utils;

namespace fs = std::filesystem;

std::optional<std::string> load_file_content(const std::filesystem::path& filename)
{
    std::ifstream in(filename, std::ios::in | std::ios::binary);

    if (in)
    {
        std::string content;
        in.seekg(0, std::ios::end);
        content.resize(static_cast<size_t>(in.tellg()));
        in.seekg(0, std::ios::beg);
        in.read(&content[0], content.size());
        in.close();
        return content;
    }

    else
        return {};
}

template <typename ExecutionPolicy>
std::uintmax_t count_words(ExecutionPolicy exec_policy, string_view text)
{
    if (text.empty())
        return 0;

    auto is_word_beginning = [](unsigned char left, unsigned char right) {
        return std::isspace(left) && !std::isspace(right);
    };

    std::uintmax_t wc = (!std::isspace(text.front()) ? 1 : 0);
    wc += std::transform_reduce(exec_policy,
        text.begin(),
        text.end() - 1,
        text.begin() + 1,
        std::uintmax_t{0},
        std::plus<>{},
        is_word_beginning);

    return wc;
}

int main()
{
    vector<int> vec(10'000'000);

    random_device rd;
    //mt19937_64 rnd_gen(rd());
    uniform_int_distribution<int> rnd_distr(0, 100);

    generate(vec.begin(), vec.end(), [&rd, &rnd_distr] { return rnd_distr(rd); });

    cout << "\n---------------------------------------\n";

    unsigned long long sum1{};

    BENCHMARK("accumulate (normal):", [&]() {
        sum1 = accumulate(vec.begin(), vec.end(), 0ULL);
    });

    std::cout << "sum1: " << sum1 << endl;

    unsigned long long sum2{};

    BENCHMARK("accumulate (par_unseq):", [&]() {
        sum2 = reduce(std::execution::par_unseq, vec.begin(), vec.end(), 0ULL);
    });

    std::cout << "sum2: " << sum2 << endl;

    cout << "\n---------------------------------------\n";

    auto vec1 = vec;

    BENCHMARK("sorting (normal):", [&] {
        sort(vec1.begin(), vec1.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    auto vec2 = vec;

    BENCHMARK("sorting (seq):", [&] {
        sort(std::execution::seq, vec2.begin(), vec2.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    auto vec3 = vec;

    BENCHMARK("sorting (par_unseq):", [&] {
        sort(std::execution::par_unseq, vec3.begin(), vec3.end());
    });

    assert(is_sorted(vec1.begin(), vec1.end()));

    cout << "\n---------------------------------------\n";

    fs::path books_dir{"./books"};
    vector<fs::directory_entry> book_files(fs::directory_iterator{books_dir}, {});

    cout << "\nBooks:\n";
    for (const auto& entry : book_files)
        cout << entry.path().filename() << "\n";

    cout << "\n";

    auto content1 = ""s;
    BENCHMARK("loading files (seq): ", [&] {
        content1 = transform_reduce(execution::seq,
            begin(book_files), end(book_files),
            ""s,
            plus<>{},
            [](auto& book_entry) { return load_file_content(book_entry.path()).value_or(""s); });
    });

    auto content2 = ""s;
    BENCHMARK("loading files (par): ", [&] {
        content2 = transform_reduce(execution::par,
            begin(book_files), end(book_files),
            ""s,
            plus<>{},
            [](auto& book_entry) { return load_file_content(book_entry.path()).value_or(""s); });
    });

    cout << "\n";

    assert(content1 == content2);

    {
        uintmax_t word_count{};

        BENCHMARK("word count (seq)", [& content = as_const(content1), &word_count] {
            word_count = count_words(std::execution::seq, content);
        });

        cout << "word_count: " << word_count << endl;
    }

    {
        uintmax_t word_count{};

        BENCHMARK("word count (par_unseq)", [& content = as_const(content2), &word_count] {
            word_count = count_words(std::execution::par_unseq, content);
        });

        cout << "word_count: " << word_count << endl;
    }
}