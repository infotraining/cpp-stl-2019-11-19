
##############
# Vcpkg integration - uncomment if necessery
if(DEFINED ENV{VCPKG_ROOT} AND NOT DEFINED CMAKE_TOOLCHAIN_FILE)
  set(CMAKE_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/scripts/buildsystems/vcpkg.cmake"
      CACHE STRING "")
endif()

message(STATUS "Vcpkg integration script found: " ${CMAKE_TOOLCHAIN_FILE})
##############

get_filename_component(PROJECT_NAME_STR ${CMAKE_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${PROJECT_NAME_STR})

cmake_minimum_required(VERSION 3.0)
project(${PROJECT_NAME_STR})

set(CMAKE_BUILD_TYPE "Release")

#----------------------------------------
# Application
#----------------------------------------
aux_source_directory(. SRC_LIST)

# Headers
file(GLOB HEADERS_LIST "*.h" "*.hpp")
add_executable(${PROJECT_NAME} ${SRC_LIST} ${HEADERS_LIST})
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)

if(MSVC)
else()
	find_package(TBB CONFIG REQUIRED)
	find_package(Threads REQUIRED)
	target_link_libraries(${PROJECT_NAME} PRIVATE TBB::tbb Threads::Threads stdc++fs)
endif()

#----------------------------------------
# Tests
#----------------------------------------
enable_testing()
add_test(tests ${PROJECT_NAME})


file(COPY proust.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY alice.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY holmes.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY pride.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY finn.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY plato.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY ulysses.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
file(COPY dracula.txt DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/books)
