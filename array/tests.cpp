#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

namespace Description
{
    template <typename T, size_t N>
    struct array
    {
        typedef T value_type;
        typedef T& reference;
        typedef T* iterator;
        typedef const T* const_iterator;

        T items_[N];

        iterator begin()
        {
            return items_;
        }

        iterator end()
        {
            return items_ + N;
        }

        constexpr size_t size() const
        {
            return N;
        }
    };
}

TEST_CASE("array")
{
    SECTION("is substitute for c-style array")
    {
        int tab1[10];
        Utils::print(tab1, "tab1");

        int tab2[3] = {1, 2, 3};
        Utils::print(tab2, "tab2");

        int tab3[10] = {5, 345, 63};
        Utils::print(tab3, "tab3");

        int tab4[10] = {};
        Utils::print(tab4, "tab4");
    }

    SECTION("as better alternative")
    {
        std::array<int, 10> arr1;
        Utils::print(arr1, "arr1");

        static_assert(is_aggregate_v<decltype(arr1)>);

        std::array<int, 3> arr2 = {1, 2, 3};

        std::array<int, 10> arr3 = {5, 343, 55};

        std::array<int, 10> arr4 = {};
    }
}

std::array<int, 3> get_coords()
{
    return {1, 2, 3};
}

TEST_CASE("std::array can be returned from function")
{
    std::array<int, 3> coords = get_coords();

    REQUIRE(coords == array {1, 2, 3});

    auto [x, y, z] = get_coords();

    REQUIRE(x == 1);
    REQUIRE(z == 3);
}

struct X
{
    int value;

    X() = default;

    X(int v)
        : value {v}
    {
    }
};

TEST_CASE("array of X")
{
    std::array<X, 4> arr = {X {1}, X {2}, X {3}};

    REQUIRE(arr[0].value == 1);
}

TEST_CASE("std::array has STL api")
{
    std::array arr = {1, 2, 3, 4};
    REQUIRE(arr.size() == 4);

    arr.fill(13);
    REQUIRE(arr == array {13, 13, 13, 13});

    REQUIRE_THROWS_AS(arr.at(5), std::out_of_range);

    array other = {6, 7, 8, 9};
    arr.swap(other);

    REQUIRE(arr == array {6, 7, 8, 9});
    REQUIRE(other == array {13, 13, 13, 13});
}

namespace Legacy
{
    void use_tab(int tab[], size_t size)
    {
        cout << "using tab: ";
        for (int* it = tab; it != tab + size; ++it)
            cout << *it << " ";
        cout << "\n";
    }
}

TEST_CASE("std::array in legacy")
{
    array arr = {1, 2, 3};
    Legacy::use_tab(arr.data(), arr.size());
}

TEST_CASE("std::array has tuple like interface")
{
    array arr = {1, 2, 3};

    REQUIRE(get<0>(arr) == 1);
    REQUIRE(arr[1] == 2);
    REQUIRE(get<2>(arr) == 3);
}

template <size_t N>
constexpr int sum(const std::array<int, N>& arr)
{
    int result {};
    for (const auto& item : arr)
        result += item;

    return result;
}

TEST_CASE("array can be used as constexpr")
{
    auto square = [](int x) { return x * x; };

    constexpr array data = {1, 4, square(3)};

    static_assert(sum(data) == 14);
}
