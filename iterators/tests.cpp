#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

template <typename InputIterator, typename OutputIterator>
OutputIterator my_copy(InputIterator first, InputIterator last, OutputIterator out)
{
    for (InputIterator it = first; it != last; ++it)
    {
        *out = *it;
        ++out;
    }

    return out;
}

template <typename InputIterator, typename OutputIterator, typename Predicate>
OutputIterator my_copy_if(InputIterator first, InputIterator last, OutputIterator out, Predicate predicate)
{
    for (InputIterator it = first; it != last; ++it)
    {
        if (predicate(*it))
        {
            *out = *it;
            ++out;
        }
    }

    return out;
}

template <typename Iterator>
void advance_it(Iterator& it, size_t n)
{
    using IteratorTag = typename iterator_traits<Iterator>::iterator_category;

    if constexpr (is_same_v<random_access_iterator_tag, IteratorTag>)
    {
        it += n;
    }
    else
    {
        for (size_t i = 0; i < n; ++i)
            ++it;
    }
}

TEST_CASE("iterators")
{
    vector<int> vec = {1, 2, 3, 4};
    list<int> lst(vec.size());

    my_copy(vec.begin(), vec.end(), lst.begin());

    for (const auto& item : lst)
        cout << item << " ";
    cout << "\n";

    static_assert(
        is_same_v<
            random_access_iterator_tag,
            iterator_traits<vector<int>::iterator>::iterator_category>);

    auto it = lst.begin();
    advance_it(it, 2);
}

TEST_CASE("insert iterators")
{
    vector vec = {1, 2, 3, 4};

    back_insert_iterator bit(vec);

    *bit = 5;
    ++bit;
    *bit = 6;
    *bit = 7;

    Utils::print(vec, "vec");

    list<int> evens;
    my_copy_if(vec.begin(), vec.end(), back_inserter(evens), [](int x) { return x % 2 == 0; });

    Utils::print(evens, "evens");
}

TEST_CASE("reverse iterators")
{
    vector vec = {1, 2, 3, 4, 5, 6, 7, 8};

    list<int> evens;

    copy_if(vec.rbegin(), vec.rend(), back_inserter(evens), [](int x) { return x % 2 == 0; });

    Utils::print(evens, "evens");
}

bool is_palindrome(string_view text)
{
    //    string::const_iterator it = text.begin();
    //    string::const_reverse_iterator rit = text.rbegin();

    //    for (; it < rit.base(); ++it, ++rit)
    //    {
    //        if (*it != *rit)
    //            return false;
    //    }

    //    return true;

    auto middle = text.begin() + text.size() / 2;
    return equal(text.begin(), middle, text.rbegin());
}

TEST_CASE("is_palindrome")
{
    REQUIRE(is_palindrome("potop"));
    REQUIRE(is_palindrome("abba"));
    REQUIRE(is_palindrome("kajak"));
    REQUIRE_FALSE(is_palindrome("kajaki"));
}

//TEST_CASE("stream iterators")
//{
//    cout << "\nstream iterators:\n";
//    cout << endl;

//    istream_iterator<int> inp_it(cin);
//    istream_iterator<int> end_it;

//    vector<int> vec(inp_it, end_it);

//    copy(begin(vec), end(vec), ostream_iterator<int>(cout, "; "));
//    cout << "\n";
//}

TEST_CASE("move iterators")
{
    using namespace Utils;

    array<Utils::Gadget, 4> gadgets = {Gadget {"ipad"}, Gadget {"playstation"}, Gadget {"mp3"}, Gadget {"kindle"}};

    vector vec_gadgets(move_iterator(begin(gadgets)), move_iterator(end(gadgets)));
}
