#include <algorithm>
#include <iostream>
#include <map>
#include <numeric>
#include <set>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

TEST_CASE("sets")
{
    set<int> set_int = {4, 25, 665, 534, 23, 66, 665, 667, 42, 13, 12, 1, 7, 665};

    Utils::print(set_int, "set_int");

    if (auto pos = set_int.find(42); pos != set_int.end())
    {
        cout << "Item found: " << *pos << "\n";
    }

    set_int.erase(42);

    Utils::print(set_int, "set_int");

    if (auto [pos, was_inserted] = set_int.insert(667); was_inserted)
    {
        cout << *pos << " was inserted!\n";
    }
    else
    {
        cout << *pos << " is already in a set\n";
    }

    set_int.emplace_hint(set_int.end(), 2018);
}

TEST_CASE("multiset")
{
    multiset<int> mset_int = {4, 25, 665, 534, 23, 66, 665, 667, 42, 13, 12, 1, 7, 665};

    for (auto it = mset_int.lower_bound(665); it != mset_int.upper_bound(665); ++it)
        cout << *it << " ";
    cout << "\n";

    {
        auto [first, last] = mset_int.equal_range(665);
        mset_int.erase(first, last);

        Utils::print(mset_int, "mset_int");
    }

    {
        auto [first, last] = mset_int.equal_range(665);

        REQUIRE(first == last);

        mset_int.emplace_hint(first, 665);
    }
}

namespace Explain
{
    template <typename T = void>
    struct Greater
    {
        bool operator()(const T& a, const T& b) const
        {
            return a > b;
        }
    };

    template <>
    struct Greater<void>
    {
        using is_transparent = bool;

        template <typename A, typename B>
        bool operator()(const A& a, const B& b) const
        {
            return a > b;
        }
    };
}

bool cmp_by_pointed_value(const unique_ptr<int>& a, const unique_ptr<int>& b)
{
    return *a < *b;
}

TEST_CASE("sorting functions")
{
    SECTION("as template parameter")
    {
        set<string, greater<>> dict_desc = {"one", "two", "three", "four"};

        Utils::print(dict_desc, "dict_desc");

        dict_desc.insert("five");

        REQUIRE(dict_desc.count("three") == 1);
    }

    SECTION("as function pointer")
    {
        set<
            unique_ptr<int>, bool (*)(const unique_ptr<int>&, const unique_ptr<int>&)>
            ptr_values(&cmp_by_pointed_value);

        ptr_values.insert(make_unique<int>(4));
        ptr_values.insert(make_unique<int>(12));
        ptr_values.insert(make_unique<int>(5));
        ptr_values.insert(make_unique<int>(66));

        for (const auto& ptr : ptr_values)
            cout << *ptr << " ";
        cout << "\n";
    }

    SECTION("as lambda")
    {
        auto cmp_by_value = [](const auto& a, const auto& b) { return *a < *b; };

        set ptrs({make_shared<int>(13), make_shared<int>(4),
                     make_shared<int>(87)},
            cmp_by_value);

        for (const auto& ptr : ptrs)
            cout << *ptr << " ";
        cout << "\n";
    }
}

TEST_CASE("maps")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {5, "five"}, {4, "four"}};

    for (const auto& [key, value] : dict)
    {
        cout << key << " - " << value << "\n";
    }

    decltype(dict)::value_type entry {6, "six"};

    dict.insert(entry);
    dict.insert(make_pair(7, "seven"));
    dict.insert(pair(8, "eight"));
    dict.emplace(9, "nine");

    REQUIRE(dict[2] == "two");
    REQUIRE(dict.size() == 8);

    REQUIRE(dict[10] == ""s);
    REQUIRE(dict.size() == 9);

    REQUIRE_THROWS_AS(dict.at(11), std::out_of_range);

    dict.insert_or_assign(7, "Seven");

    multimap<int, string> multi_dict(dict.begin(), dict.end());

    multi_dict.emplace(7, "Siedem");

    auto [first_7, last_7] = multi_dict.equal_range(7);

    for (auto it = first_7; it != last_7; ++it)
    {
        cout << it->second << " ";
    }
    cout << "\n";
}

TEST_CASE("transfer nodes & merge")
{
    set<string> dict1 = {"one", "two", "four"};
    set<string> dict2 = {"three", "five", "four"};

    Utils::print(dict1, "dict1");
    Utils::print(dict2, "dict2");

    auto node_2 = dict1.extract("two");
    dict2.insert(std::move(node_2));

    cout << "\n After extract & insert:\n";

    Utils::print(dict1, "dict1");
    Utils::print(dict2, "dict2");

    dict2.merge(dict1);

    cout << "After merge:\n";

    Utils::print(dict1, "dict1");
    Utils::print(dict2, "dict2");
}
