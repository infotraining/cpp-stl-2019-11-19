#include <algorithm>
#include <array>
#include <deque>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

template <typename Container>
void print_stats(const Container& container, string_view prefix)
{
    cout << prefix << " - size: " << container.size() << "; capacity: " << container.capacity() << "\n";
}

TEST_CASE("vector")
{
    SECTION("contructor")
    {
        SECTION("default")
        {
            vector<int> vec;

            Utils::print(vec, "vec");
            print_stats(vec, "vec");
        }

        SECTION("with size")
        {
            vector<int> vec(10);
            Utils::print(vec, "vec");
            print_stats(vec, "vec");
        }

        SECTION("with range")
        {
            array data = {1, 2, 3, 4, 5};
            vector vec(begin(data), end(data));
            Utils::print(vec, "vec from range");
            print_stats(vec, "vec from range");
        }
    }
}

TEST_CASE("push_backs")
{
    using namespace Utils;

    {
        vector<Gadget> gadgets;

        gadgets.reserve(30);

        for (int i = 0; i < 32; ++i)
        {
            gadgets.emplace_back("g");
            print_stats(gadgets, "gadgets after " + to_string(i + 1) + " pb: ");
        }

        gadgets.clear();
        gadgets.reserve(10);

        print_stats(gadgets, "gadgets after clear");

        gadgets.shrink_to_fit();

        print_stats(gadgets, "gadgets after shrink_to_fit");
    }
}

TEST_CASE("invalidating iterators")
{
    vector vec = {1, 4, 3, 4, 5, 6, 4};

    int& last = vec.back();

    int* ptr = &vec[4];

    auto pos = find(begin(vec), end(vec), 6);

    //vec.erase(vec.end() - 1);

    vec.push_back(8);

    //    for (auto it = vec.begin(); it != vec.end();)
    //        if (*it == 4)
    //            it = vec.erase(it);
    //        else
    //            ++it;

    auto new_end = remove(begin(vec), end(vec), 4);
    vec.erase(new_end, vec.end());

    Utils::print(vec, "vec after remove");
}

TEST_CASE("erase")
{
    vector vec = {1, 2, 3, 4};

    SECTION("linear time - O(N)")
    {
        auto pos = vec.begin();

        vec.erase(pos);
    }

    SECTION("constant time - O(1)")
    {
        auto pos = vec.begin();

        auto efficient_erase = [](auto& vec, auto pos) {
            *pos = std::move(vec.back());
            vec.pop_back();
        };

        efficient_erase(vec, pos);
    }
}

TEST_CASE("vector<bool>")
{
    vector<bool> vec_flags = {0, 1, 1, 1, 0, 1};

    Utils::print(vec_flags, "vec_flags");

    REQUIRE(vec_flags[0] == 0);

    vec_flags[0].flip();
    vec_flags.flip();
    Utils::print(vec_flags, "vec_flags");

    vector<int> vec = {1, 2, 3};
    int* ptr = &vec[0];

    //bool* ptr_flag = &vec_flags[0];

    for (auto&& flag : vec_flags)
        flag.flip();
}

TEST_CASE("deque")
{
    deque<int> dq1 = {1, 2, 3, 4, 5, 6};

    int& item = dq1[3]; // item 4

    dq1.push_front(0);
    dq1.push_back(7);

    Utils::print(dq1, "dq1");

    REQUIRE(dq1[4] == 4);

    dq1.pop_back();
    dq1.pop_front();

    Utils::print(dq1, "dq1");

    REQUIRE(item == 4);

    dq1.clear();
}

TEST_CASE("list")
{
    list<int> lst1 = {1, 23, 23, 1235, 2345, 665, 42, 665, 1324, 13, 234, 665};
    Utils::print(lst1, "lst1");

    lst1.insert(std::next(lst1.begin(), 4), -1);
    Utils::print(lst1, "lst1");

    list lst2 = {6, 2435, 665, 345, 34356, 42};
    Utils::print(lst2, "lst2");

    lst1.sort();
    lst2.sort();

    Utils::print(lst1, "lst1");
    Utils::print(lst2, "lst2");

    lst1.merge(lst2);

    Utils::print(lst1, "lst1");
    Utils::print(lst2, "lst2");

    lst1.unique();
    Utils::print(lst1, "lst1");

    SECTION("splice")
    {
        using namespace Utils;

        Gadget::clear_stats();

        list<Gadget> gadgets1;
        gadgets1.emplace_back("g1");
        gadgets1.emplace_back("g2");
        gadgets1.emplace_back("g3");
        gadgets1.emplace_back("g4");

        list<Gadget> gadgets2;
        gadgets2.emplace_back("g6");
        gadgets2.emplace_front("g5");

        print(gadgets1, "gadgets1");
        print(gadgets2, "gadgets2");

        auto first = std::next(gadgets1.begin(), 1);
        auto last = std::next(gadgets1.begin(), 3);

        gadgets2.splice(std::next(gadgets2.begin(), 1), gadgets1, first, last);

        cout << "\nafter splice:\n";
        print(gadgets1, "gadgets1");
        print(gadgets2, "gadgets2");

        Gadget::print_stats();
    }
}
