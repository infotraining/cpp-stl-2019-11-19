#include <iostream>
#include <string>
#include <string_view>

namespace Utils
{
    template <typename Container>
    void print(const Container& container, std::string_view prefix)
    {
        std::cout << prefix << ": [ ";
        for (const auto& item : container)
            std::cout << item << " ";
        std::cout << "]" << std::endl;
    }

    class Gadget
    {
        std::uint64_t id_;
        std::string name_;

        static uint64_t gen_id()
        {

            return ++id_seed;
        }

        inline static std::uint64_t id_seed{};
        inline static std::uint64_t copy_constructor_count{};
        inline static std::uint64_t move_constructor_count{};
        inline static std::uint64_t copy_assignment_count{};
        inline static std::uint64_t move_assignment_count{};
        inline static bool silent_mode{false};

    public:
        static void print_stats()
        {
            std::cout << "-------------------------------\n";
            std::cout << "constructed: " << id_seed << "\n";
            std::cout << "copy constructed: " << copy_constructor_count << "\n";
            std::cout << "move constructed: " << move_constructor_count << "\n";
            std::cout << "copy assigned: " << copy_assignment_count << "\n";
            std::cout << "move assigned: " << move_assignment_count << "\n";
            std::cout << "-------------------------------\n";
        }

        static void clear_stats()
        {
            id_seed = 0;
            copy_constructor_count = 0;
            copy_assignment_count = 0;
            move_constructor_count = 0;
            move_assignment_count = 0;
        }

        Gadget()
            : id_{gen_id()}
            , name_{std::string("default") + std::to_string(id_)}
        {
            if (!silent_mode)
                std::cout << "Gadget(" << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget(const std::string& name)
            : id_{gen_id()}
            , name_{name}
        {
            if (!silent_mode)
                std::cout << "Gadget(" << id_ << ", " << name_ << ")" << std::endl;
        }

        Gadget(const Gadget& source)
            : id_{source.id_}
            , name_{source.name_}
        {
            if (!silent_mode)
                std::cout << "Gadget(cc: " << id_ << ", " << name_ << ")" << std::endl;
            ++copy_constructor_count;
        }

        Gadget(Gadget&& source) noexcept
            : id_ {source.id_}
            , name_ {std::move(source.name_)}
        {
            if (!silent_mode)
                std::cout << "Gadget(mv: " << id_ << ", " << name_ << ")" << std::endl;
            ++move_constructor_count;
        }

        Gadget& operator=(const Gadget& source)
        {
            if (this != &source)
            {
                id_ = source.id_;
                name_ = source.name_;
            }

            if (!silent_mode)
                std::cout << "Gadget(c=: " << id_ << ", " << name_ << ")" << std::endl;

            ++copy_assignment_count;

            return *this;
        }

        Gadget& operator=(Gadget&& source)
        {
            if (this != &source)
            {
                id_ = source.id_;
                name_ = std::move(source.name_);
            }

            if (!silent_mode)
                std::cout << "Gadget(m=: " << id_ << ", " << name_ << ")" << std::endl;

            ++move_assignment_count;

            return *this;
        }

        uint64_t id() const
        {
            return id_;
        }

        std::string name() const
        {
            return name_;
        }
    };

    inline std::ostream& operator<<(std::ostream& out, const Gadget& g)
    {
        out << "Gadget{id: " << g.id() << ", name: " << g.name() << "}";
        return out;
    }
}

class Wrapper
{
    Utils::Gadget g;

    Wrapper(Wrapper&&) = default; // implicitly noexcept

    virtual ~Wrapper() = default;
};
