# Standard Template Library

## Doc

* https://infotraining.bitbucket.io/cpp-stl


## Virtual machine - settings

### login and password for VM:

```
dev  /  pwd
```

### reinstall VBox addon (optional)

```
sudo /etc/init.d/vboxadd setup
```

### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

## GIT

```
git clone https://infotraining-dev@bitbucket.org/infotraining/cpp-stl-2019-11-19.git
```

## Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [compiler explorer](https://gcc.godbolt.org/)

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-stl-2019-11-19-kp

