#include <algorithm>
#include <execution>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "catch.hpp"
#include "utils.hpp"

using namespace std;

namespace Explain
{
    template <typename InputIterator, typename UnaryFunc>
    UnaryFunc for_each(InputIterator first, InputIterator last, UnaryFunc f)
    {
        for (auto it = first; it != last; ++it)
            f(*it);

        return f;
    }
}

inline void print(int x)
{
    cout << x << " ";
}

struct Counter
{
    int value {};

    void operator()(int x)
    {
        value += x;
    }
};

TEST_CASE("function vs functors vs lambda")
{
    vector vec = {1, 2, 3, 4};

    SECTION("function")
    {
        Explain::for_each(vec.begin(), vec.end(), print);
        cout << "\n";
    }

    SECTION("functor")
    {
        auto counter = Explain::for_each(vec.begin(), vec.end(), Counter {});

        REQUIRE(counter.value == 10);
    }

    SECTION("lambda")
    {
        int sum {};

        for_each(vec.begin(), vec.end(), [&sum](int x) { sum += x; });

        REQUIRE(sum == 10);
    }

    SECTION("std::function - avoid")
    {
        function f = print;

        for_each(vec.begin(), vec.end(), f);
    }
}

TEST_CASE("std functors")
{
    vector vec1 {1, 2, 3};
    vector vec2 {3, 2, 1};

    vector<int> dest(vec1.size());

    transform(begin(vec1), end(vec1), begin(vec2), begin(dest), plus {});
    REQUIRE(dest == vector {4, 4, 4});

    multiplies mp;
    REQUIRE(mp(3, 2) == 6);

    transform(begin(vec1), end(vec1), begin(dest), negate {});
    REQUIRE(dest == vector {-1, -2, -3});

    vector data {6, 234, 756, 665, 2, 42, 23456};
    sort(begin(data), end(data), greater {});

    Utils::print(data, "data");
}

struct Person
{
    string name;

    explicit Person(string name)
        : name {std::move(name)}
    {
    }

    void print() const
    {
        cout << "Person(" << name << ")\n";
    }
};

TEST_CASE("std::mem_fn")
{
    cout << "\npeople:\n";
    vector people = {Person {"Jan"}, Person {"Ewa"}, Person {"Adam"}};
    for_each(begin(people), end(people), std::mem_fn(&Person::print));

    cout << "\npeople ptrs:\n";
    vector people_ptrs = {&people[0], &people[1], &people[2]};
    for_each(begin(people_ptrs), end(people_ptrs), std::mem_fn(&Person::print));

    cout << "\npeople shared_ptrs:\n";
    vector people_sp = {make_shared<Person>("Jan"), make_shared<Person>("Ewa")};
    for_each(begin(people_sp), end(people_sp), std::mem_fn(&Person::print));
}

TEST_CASE("non modifing algorithms")
{
    vector vec = {3, 23534, 3, 7, 245, 42, 7, 254, 6, 24};

    REQUIRE(count(begin(vec), end(vec), 7) == 2);

    REQUIRE(count_if(begin(vec), end(vec), [](int x) { return x > 200; }) == 3);

    auto is_even = [](int x) { return x % 2 == 0; };

    REQUIRE_FALSE(all_of(begin(vec), end(vec), is_even));
    REQUIRE(any_of(begin(vec), end(vec), is_even));
}

TEST_CASE("equal & compare")
{
    vector vec = {1, 2, 3};
    list lst = {1, 2, 3};

    REQUIRE(equal(begin(vec), end(vec), begin(lst), end(lst)));

    list lst2 {1, 3, 1};

    REQUIRE(lexicographical_compare(begin(vec), end(vec), begin(lst2), end(lst2)));

    SECTION("min max")
    {
        vector vec = {3, 23534, 3, 7, 245, 42, 7, 254, 6, 24};

        auto [pos_min, pos_max] = minmax_element(begin(vec), end(vec));
        REQUIRE(*pos_min == 3);
        REQUIRE(*pos_max == 23534);
    }
}

TEST_CASE("find et al")
{
    vector vec = {3, 23534, 3, 7, 245, 42, 42, 7, 254, 6, 24};

    auto twins_pos = adjacent_find(begin(vec), end(vec));

    REQUIRE(*twins_pos == 42);
    REQUIRE(*twins_pos == *(twins_pos + 1));

    if (auto pos = find_if(begin(vec), end(vec), [](int x) { return x > 1000; }); pos != end(vec))
    {
        cout << "Item found: " << *pos << "\n";
    }
    else
    {
        cout << "Item not found!\n";
    }
}

TEST_CASE("binary search")
{
    vector vec = {3, 23534, 3, 7, 245, 42, 42, 7, 254, 6, 24};

    sort(execution::par_unseq, begin(vec), end(vec));

    REQUIRE(binary_search(begin(vec), end(vec), 254));

    auto [lb, ub] = equal_range(begin(vec), end(vec), 665);

    REQUIRE(lb == ub);

    vec.insert(lb, 665);

    Utils::print(vec, "vec");
}

auto create_generator(size_t seed, size_t delta = 1)
{
    return [seed, delta]() mutable { size_t tmp = seed; seed += delta; return tmp; };
}

TEST_CASE("modifing algorithms")
{
    vector vec = {3, 23534, 3, 7, 245, 42, 42, 7, 254, 6, 24};

    vector<int> dest;

    copy_n(rbegin(vec), 5, back_inserter(dest));

    fill(begin(vec), end(vec), 0);
    Utils::print(vec, "vec");

    iota(rbegin(vec), rend(vec), 0);
    Utils::print(vec, "vec");

    generate(begin(vec), end(vec), create_generator(0, 5));
    Utils::print(vec, "vec");

    random_device rd;
    mt19937 rnd_gen(rd());
    shuffle(begin(vec), end(vec), rnd_gen);
    Utils::print(vec, "vec");

    auto new_end = remove_if(begin(vec), end(vec), [](int x) { return x > 20; });
    vec.erase(new_end, vec.end());
    Utils::print(vec, "vec");

    vector data = {1, 5, 345, 6, 7, 6, 6, 324, 645, 6};
    sort(begin(data), end(data));
    new_end = unique(begin(data), end(data));
    data.erase(new_end, data.end());
    Utils::print(data, "data");
}

TEST_CASE("sorting")
{
    SECTION("stable sort")
    {
        vector data = {tuple(1, "a"), tuple(2, "b"), tuple(3, "b"), tuple(4, "a"), tuple(5, "b"), tuple(6, "c")};

        stable_sort(begin(data), end(data), [](const auto& a, const auto& b) { return get<1>(a) < get<1>(b); });

        for (const auto& [id, name] : data)
            cout << id << " - " << name << "\n";
    }

    vector<int> vec(20);

    random_device rd;
    mt19937 rnd_gen(rd());
    uniform_int_distribution<> rnd_dstr(0, 100);

    generate(begin(vec), end(vec), [&rnd_dstr, &rnd_gen] { return rnd_dstr(rnd_gen); });

    Utils::print(vec, "vec");

    nth_element(begin(vec), begin(vec) + 5, end(vec), greater {});

    copy_n(begin(vec), 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    partial_sort(begin(vec), begin(vec) + 5, end(vec), greater {});
    copy_n(begin(vec), 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    vector<int> largest(5);
    partial_sort_copy(begin(vec), end(vec), begin(largest), end(largest), greater {});
    Utils::print(largest, "largest");

    auto threshold_50 = partition(begin(vec), end(vec), [](int x) { return x < 50; });

    copy(begin(vec), threshold_50, ostream_iterator<int>(cout, " "));
    cout << "\n";
}

TEST_CASE("merge")
{
    vector vec1 = {1, 23, 5, 34, 5, 236, 664};
    vector vec2 = {1, 23, 665, 34, 7, 6, 236, 664};

    sort(vec1.begin(), vec1.end());
    sort(vec2.begin(), vec2.end());

    vector<int> merged_vec(vec1.size() + vec2.size());

    merge(begin(vec1), end(vec1), begin(vec2), end(vec2), begin(merged_vec));
    Utils::print(merged_vec, "merged vec");

    vector data = {665, 45, 12, 7, 664};
    sort(begin(data), end(data));

    vector<int> intersection;
    set_intersection(begin(merged_vec), end(merged_vec), begin(data), end(data), back_inserter(intersection));
    Utils::print(intersection, "intersection");
}

TEST_CASE("accumulate")
{
    vector vec = {5.0, 5.6, 3.14, 8.88};

    auto result = accumulate(begin(vec), end(vec), 1.0, multiplies {});

    cout << "result: " << result << "\n";

    vector<string> words = {"one", "two", "three", "four"};

    auto total_length = accumulate(begin(words), end(words),
        0,
        [](int length, const string& s) { return length + s.size(); });

    cout << "total length: " << total_length << "\n";

    auto reduced_length = transform_reduce(execution::seq,
        begin(words),
        end(words),
        0,
        plus {},
        [](const auto& s) { return s.size(); });

    REQUIRE(total_length == reduced_length);

    SECTION("count_if & accumulate")
    {
        vector vec = {1, 2, 5, 6, 7, 7, 1};

        auto count_if_acc = [](auto first, auto last, auto pred) {
            return accumulate(first, last, 0u, [&pred](size_t cnt, const auto& item) {
                return cnt + (pred(item) ? 1 : 0);
            });
        };

        auto gt_5 = [](int x) { return x > 5; };
        REQUIRE(count_if(begin(vec), end(vec), gt_5) == count_if_acc(begin(vec), end(vec), gt_5));
    }
}
